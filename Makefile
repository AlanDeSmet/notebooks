
INPUTS=$(wildcard public/*.ipynb)
OUTPUTS=$(patsubst %.ipynb,%.html,$(INPUTS))

all: $(OUTPUTS) public/index.html

public/%.html: public/%.ipynb
	jupyter nbconvert $< --output-dir=public 

public/index.html:
	./mkindex > public/index.html
